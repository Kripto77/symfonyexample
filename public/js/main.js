$(function () {
    $.ajaxSetup({
        type: "POST",
        url: "/ajax",
        cache: false
    });

    $(document).on('change keyup', '.juro-input input, .juro-select select', function () {
        let p = $(this).parent(), val = $(this).val();
        val ==='' || val === '0' ? p.removeClass('juro-filled') : p.addClass('juro-filled').removeClass('juro-error');
    });

    /* При загрузке проверяем если поле заполненно - добавляем filled */
    $('.juro-input input, .juro-select select').each(function(){
        let j = $(this).parent(), val = $(this).val();
        val ==='' || val === '0'/*&& !$(this).attr('placeholder')*/ ? j.removeClass('juro-filled') :  j.addClass('juro-filled');
    });

});
