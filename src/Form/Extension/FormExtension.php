<?php
/**
 */

namespace App\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
#use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\FormType;

class FormExtension extends AbstractTypeExtension
{
    /**
     * Return the class of the type being extended.
     */
    public static function getExtendedTypes(): iterable
    {
        // return FormType::class to modify (nearly) every field in the system
        return [FormType::class];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(array('block_class'));
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['block_class'] = isset($options['block_class']) ? $options['block_class'] : '';

        /*if (isset($options['block_class'])) {
            // будет тем классом / сущностью, которая привязана к вашей форме (например, Media)
            $parentData = $form->getParent()->getData();

            $blockClass = null;
            if (null !== $parentData) {
                $accessor = PropertyAccess::createPropertyAccessor();
                $blockClass = $accessor->getValue($parentData, $options['block_class']);
            }

            // устанавливает переменную "image_url", которая будет доступна при отображении этого поля
            $view->vars['block_class'] = $blockClass;
        }*/
    }
}