<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form;

use App\Entity\User;
#use App\Service\HelperService;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Defines the form used to edit an user.
 *
 * @author Romain Monteil <monteil.romain@gmail.com>
 */
class UserType extends AbstractType
{
    private $helperService;
    private $parameterBag;

    public function __construct(ParameterBagInterface $parameterBag/*, HelperService $helperService*/)
    {
        #$this->helperService = $helperService;
        $this->parameterBag = $parameterBag;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // For the full reference of options defined by each form field type
        // see https://symfony.com/doc/current/reference/forms/types.html

        // By default, form fields include the 'required' attribute, which enables
        // the client-side form validation. This means that you can't test the
        // server-side validation errors from the browser. To temporarily disable
        // this validation, set the 'required' attribute to 'false':
        // $builder->add('title', null, ['required' => false, ...]);

        $builder
            ->add('name', TextType::class, [
                'label' => 'label.name',
                'block_class' => 'juro-block juro-input',
            ])
            ->add('email', EmailType::class, [
                'label' => 'label.email',
                'block_class' => 'juro-block juro-input',
            ])
            /*->add('roles', RolesTreeType::class, [
                'label' => 'label.email',
                'block_class' => 'juro-block juro-input',
            ])*/
            ->add('plainPassword', PasswordType::class, [
                'block_class' => 'juro-block juro-input',
                'label' => 'label.password',
                // Если не надо добавлять в сущность User
                #'mapped' => false,
                'required' => false,
                'constraints' => [
                    /*new NotBlank([
                        'message' => 'Please enter a password',
                    ]),*/
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            //TODO: Нежелательно разрешать левые поля данных - кнопки сохранения
            'allow_extra_fields' => true,
        ]);
    }

    private function getArrayTreeUserRoles()
    {
        $roles = $this->parameterBag->get('security.role_hierarchy.roles');
        //foreach($roles as $role)
        return $roles;

    }
}
