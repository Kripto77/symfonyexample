<?php

namespace App\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RouterInterface;

class KernelRequestListener
{
    private $entityManager;
    private $security;
    private $router;

    public function __construct(Container $container, EntityManagerInterface $entityManager,
                                Security $security, RouterInterface $router)
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
        $this->router = $router;
    }

    public function onKernelRequest(RequestEvent $event)
    {
        $this->updateUserLoggedAt();

        if ($this->security->getUser() && $event->isMasterRequest()) {
            $currentRoute = $event->getRequest()->attributes->get('_route');

            if ($this->isAuthenticatedUserOnAnonymousPage($currentRoute)) {
                $response = new RedirectResponse($this->router->generate('app_user_cpanel'));
                $event->setResponse($response);
            }
        }
    }

    /**
     * Check if last activity of user more than N min ago - update logged_at
     */
    public function updateUserLoggedAt(): self
    {
        $user = $this->security->getUser();

        if(empty($user)) return $this;

        $now = new \DateTime(); // текущее время на сервере
        $interval = $now->diff($user->getLoggedAt());

        if($interval->i > 5) {
            $user->setLoggedAt(new \DateTime());

            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }

        return $this;
    }

    /**
     * If user already logged - redirect to dashboard
     * @param string $currentRoute
     * @return bool
     */
    private function isAuthenticatedUserOnAnonymousPage(string $currentRoute)
    {
        return in_array(
            $currentRoute,
            ['app_user_login', 'app_user_registration']
        );
    }

    /**

    public function onAuthenticationSuccess()
    {
        $user = $this->security->getUser();

        $user->setLoggedAt(new \DateTime());

        $this->entityManager->persist($user);
        $this->entityManager->flush();

    }*/

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return array(
            #AuthenticationEvents::AUTHENTICATION_FAILURE => 'onAuthenticationFailure',
            AuthenticationEvents::AUTHENTICATION_SUCCESS => 'onAuthenticationSuccess',
            #SecurityEvents::INTERACTIVE_LOGIN => 'onInteractiveLogin',
        );
    }
}