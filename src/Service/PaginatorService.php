<?php
/**
 * Pagination class
 */

namespace App\Service;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class PaginatorService
{
    private $requestStack;
    private $parameterBag;

    /** Default pagination params - setup in defaultParams() */
    private $params = [
            'currentPage' => 0,
            'itemsPerPage' => 0,
            'routeName' => '',
    ];


    public function __construct(RequestStack $requestStack, ParameterBagInterface $parameterBag)
    {
        $this->requestStack = $requestStack;
        $this->parameterBag = $parameterBag;

        $this->defaultParams();
    }

    /**
     * Creates pagination for any controller
     * @param QueryBuilder $queryBuilder Prepared query without getQuery
     * @param array $params
     * @return array
     */
    public function createPaginator(QueryBuilder $queryBuilder, array $params = []): array
    {
        $this->mergeParams($params);

        $currentPage = $this->params['currentPage'];
        $itemsPerPage = $this->params['itemsPerPage'];

        $firstResult = ($currentPage - 1) * $itemsPerPage;

        $query = $queryBuilder
            ->setFirstResult($firstResult)
            ->setMaxResults($itemsPerPage)
            ->getQuery();

        $paginator = new Paginator($query);
        $numResults = $paginator->count();
        $hasPreviousPage = $currentPage > 1;
        $hasNextPage = ($currentPage * $itemsPerPage) < $numResults;
        $totalPages = (int) ceil($numResults / $itemsPerPage);

        $countShowPages = 5;
        /* Дальше идёт вычисление первой выводимой страницы и последней (чтобы текущая страница была где-то посредине, если это возможно,
        и чтобы общая сумма выводимых страниц была равна count_show_pages, либо меньше, если количество страниц недостаточно) */
        $totalPages = ceil($numResults / $itemsPerPage);

        $left = $currentPage - 1;
        #$right = $totalPages - $currentPage;
        if($left < floor($countShowPages / 2))
            $startFromPage = 1;
        else
            $startFromPage = $currentPage - floor($countShowPages / 2);
        $stopOnPage = $startFromPage + $countShowPages - 1;
        if ($stopOnPage > $totalPages) {
            $startFromPage -= ($stopOnPage - $totalPages);
            $stopOnPage = $totalPages;
            if ($startFromPage < 1)
                $startFromPage = 1;
        }

        # Left points
        $leftPoints = 0;
        if($startFromPage > $countShowPages / 2 ) {
            $leftPoints = 1;
        }
        elseif($startFromPage == floor($countShowPages / 2) ) {
            $startFromPage--;
        }

        # Right points
        $rightPoints = 0;
        if($totalPages - $currentPage > ceil($countShowPages / 2) ) {
            $rightPoints = 1;
        }
        elseif($totalPages - $currentPage == ceil($countShowPages / 2) ) {
            $stopOnPage++;
        }

        return [
            'items' => $paginator->getIterator(),
            'routeName' => $this->params['routeName'],
            'currentPage' => $currentPage,
            'startFromPage' => $startFromPage,
            'stopOnPage' => $stopOnPage,
            'leftPoints' => $leftPoints,
            'rightPoints' => $rightPoints,
            'queryString' => $this->getQueryStringArray(),

            'hasPreviousPage' => $hasPreviousPage,
            'hasNextPage' => $hasNextPage,
            'previousPage' => $hasPreviousPage ? $currentPage - 1 : null,
            'nextPage' => $hasNextPage ? $currentPage + 1 : null,
            'numPages' => $totalPages,
            'haveToPaginate' => $numResults > $itemsPerPage,
        ];
    }

    /**
     * Get all $_GET query string in array
     * @return array
     */
    private function getQueryStringArray()
    {
        return $this->requestStack->getMasterRequest()->query->all();
    }

    /**
     * Get current page
     * @param int $currentPage
     * @return int

    private function getCurrentPage(): int
    {
        $currentPage = $this->params['currentPage'];

        if($currentPage < 1 && $getCurrentPage > 0)
            $currentPage = $getCurrentPage;

        if($currentPage < 1)
            $currentPage = 1;

        return $currentPage;        
    }

    /**
     * Get number of items per page
     * @param int $pageItems
     * @return int

    private function getPageItems(int $pageItems): int
    {
        $getPageItems = (int)$this->parameterBag->get('default_items_per_page');

        return $pageItems > 1 ? $pageItems : $getPageItems;
    }

    /**
     * Create array with default params
     */
    private function defaultParams()
    {
        $this->params = [
            'currentPage' => (int)$this->requestStack->getCurrentRequest()->get('page', 1),
            'itemsPerPage' => (int)$this->parameterBag->get('default_items_per_page'),
            'routeName' => $this->requestStack->getCurrentRequest()->get('_route'),
        ];
    }

    /**
     * Merge input params array with default params
     * @param array $params
     */
    private function mergeParams(array $params): void
    {
        #Check if exists default param - then replace with input value
        foreach($params as $key => $value)
            if(!empty($this->params[$key]))
                $this->params = [$key] = $value;
    }
}