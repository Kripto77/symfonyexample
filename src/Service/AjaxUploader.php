<?php
/**
 * Ajax uploader by Odeln
 * Need LiipImagineBundle
 */

namespace App\Service;

#use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
#use Symfony\Component\HttpFoundation\Request;
#use Liip\ImagineBundle\Imagine\Data\DataManager;
#use Liip\ImagineBundle\Imagine\Filter\FilterManager;

use Imagine\Gd\Imagine;
use Imagine\Image\Box;

class AjaxUploader
{
    private $parameterBag;
    private $request;
    private $tmpAbsPath;
    private $uploadedFile;
    private $errorMessages;

    private $ajaxUploaderOptions = [
        'ajaxUploadUrl' => '',
        'maxImages' => 5,
        'maxSize' => 25 * 1024 * 1024, #25 MB
        'photoCount' => 0,
    ];

    private $imageUploadOptions = [
        'maxWidth' => 1024,
        'maxHeight' => 768,
        'typesAllowed' => [1, 2, 3],
    ];

    public function __construct(ParameterBagInterface $parameterBag)
    {
        #$this->request = $request;
        $this->parameterBag = $parameterBag;
        $this->tmpAbsPath = $parameterBag->get('public_dir') . $parameterBag->get('path.tmp_dir') . '/';

        //TODO: Get image from $_FILES through the container (Request, ParameterBag, etc)
        $this->uploadedFile = isset($_FILES['file']) ? $_FILES['file'] : null;
    }

    /**
     * Call when ajax image upload
     * @param array $imageUploadOptions
     * @return array
     */
    public function ajaxImageUpload(array $imageUploadOptions = []): array
    {
        $this->setImageUploaderOptions($imageUploadOptions);

        if(!$this->checkUploadImage())
            return $this->resultFailure();

        return $this->processUploadedImage();

    }

    /**
     * Process uploaded image
     */
    private function processUploadedImage()
    {
        #$file = $this->rootDir . '/public/uploads/product/test.jpg';
        #$this->uploadedFile['tmp_name'] = $file;

        $bigImage = md5(uniqid(mt_rand(0, 1000))).'.jpg';
        $thumbImage = 'thumb-'.$bigImage;
        #$cacheManager->getBrowserPath($file, 'product_big_image');

        $imagine = new Imagine();

        $image = $imagine->open($this->uploadedFile['tmp_name']);

        $big = $image->thumbnail(new Box(1600, 1200));;
        $big->save($this->tmpAbsPath . $bigImage);

        $thumbnail = $image->thumbnail(new Box(400, 400));
        $thumbnail->save($this->tmpAbsPath . $thumbImage);

        $successResult = [
            'thumb' => $this->parameterBag->get('path.tmp_dir') . '/' . $thumbImage,
            'big' => $this->parameterBag->get('path.tmp_dir') . '/' . $bigImage,
        ];

        return $this->checkUploadImage($this->tmpAbsPath . $bigImage)
            ? $this->resultSuccess($successResult)
                : $this->resultFailure(['errors' => ['Error uploading image']]);

    }


    /**
     * Check uploaded image
     * @param string $path
     * @return bool
     */
    public function checkUploadImage(string $path = ''): bool
    {
        $path = empty($path) ? $this->uploadedFile['tmp_name'] : $path;

        if(!$this->uploadedFile) {
            $this->errorMessage('No image loaded');
            return false;
        }

        if($this->uploadedFile['size'] > $this->ajaxUploaderOptions['maxSize'])
            $this->errorMessage('Maximum file size 25MB');

        if(!in_array(exif_imagetype($path), [1, 2, 3]))
            $this->errorMessage('Image type not allowed');

        return sizeof($this->errorMessages) ? false : true;
    }


    /**
     * @param array $result
     * @return array
     */
    public function resultSuccess(array $result = []): array
    {
        return ['result' => true] + $result;
    }

    /**
     * @param array $result
     * @return array
     */
    public function resultFailure(array $result = []): array
    {
        $errors = sizeof($this->errorMessages) ? ['errors' => $this->errorMessages] : [];

        return ['result' => false] + $errors + $result;
    }

    /**
     * Add error message
     * @param string $errorMessage
     */
    public function errorMessage(string $errorMessage = '')
    {
        $this->errorMessages[] = $errorMessage;
    }

    /**
     * Return options for javascript settings in twig template
     * @param array $ajaxUploaderOptions
     * @return array
     */
    public function getUploaderOptions(array $ajaxUploaderOptions = []): array
    {
        foreach($ajaxUploaderOptions as $k => $v)
            if(isset($this->ajaxUploaderOptions[$k]))
                $this->ajaxUploaderOptions[$k] = $v;

        return $this->ajaxUploaderOptions;
    }

    /**
     * Rewrite image uploade options
     * @param array $imageUploadOptions
     */
    public function setImageUploaderOptions(array $imageUploadOptions = []): void
    {
        $this->errorMessages = [];

        foreach($imageUploadOptions as $k => $v)
            if(isset($this->imageUploadOptions[$k]))
                $this->imageUploadOptions[$k] = $v;
    }

}