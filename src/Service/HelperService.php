<?php
/**
 * Service class for operation with categories.
 * Need to extends AbstractCotroller for use render methods.
 */

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Category;
use App\Entity\State;


class HelperService extends AbstractController
{
    private $em;
    private $parameterBag;
    private $publicDir;
    private $categoryXref;
    private $categoryName;
    private $categorySlug;
    private $currentCategoryId;
    private $level = 0;
    private $htmlMainMenu = '';
    private $sidebarMenu = '';
    private $allChildCategories = [];
    private $states = [];
    private $sellerCategories = [];

    private $siteName = 'MegaPlatter.org';
    private $meta = ['title'=>'', 'description'=>'', 'h1'=>''];

    public function __construct(EntityManagerInterface $em, ParameterBagInterface $parameterBag)
    {
        $this->em = $em;
        $this->parameterBag = $parameterBag;

        $this->publicDir = $this->parameterBag->get('public_dir');

        $this->getAllCategories();
    }

    /**
     * @param int $category_id
     * @return string
     */
    public function getCategoryNameById(int $category_id): string
    {
        return !empty($this->categoryName[$category_id]) ? $this->categoryName[$category_id] : '';
    }

    /**
     * @param string $slug
     * @param int $current
     * @return int
     */
    public function getCategoryIdBySlug(string $slug, int $current = 0): int
    {
        $categoryId = array_search($slug, $this->categorySlug);

        if($current)
            $this->currentCategoryId = $categoryId;

        return is_int($categoryId) ? $categoryId : 0;
    }

    /**
     * @param int $categoryId
     * @return string
     */
    public function getCategoryUrl(int $categoryId): string
    {
        $arr = [];
        $arr[] = $this->categorySlug[$categoryId];
        while($parentId = $this->categoryXref[$categoryId]) {
            if(!empty($this->categorySlug[$parentId]))
                $arr[] = $this->categorySlug[$parentId];
            $categoryId = $parentId;
        }

        $arr  = array_merge([''], array_reverse($arr), ['']);

        return implode('/', $arr);
    }

    /**
     * Render head section template for page meta data
     */
    public function renderHeadSection()
    {
        $title = !empty($this->meta['title']) ? $this->meta['title'] . ' - ' . $this->siteName : $this->siteName;

        return $this->render('system/head-section.html.twig', [
            'title' => $title,
            'description'=> $this->meta['description']
        ]);
    }

    /**
     * Set meta data for current page
     * @param array $meta
     */
    public function setMetaData(array $meta)
    {
        array_walk($meta, function (&$v, $k) { $v = filter_var($v, FILTER_SANITIZE_STRING); });
        $this->meta = array_merge($this->meta, $meta);
    }


    /**
     * Partially rendering main menu
     * @return Response
     */
    public function getMainMenuTree()
    {
        if(empty($this->htmlMainMenu))
            $this->renderChildCategoriesByRecursive(0);

        return new Response($this->htmlMainMenu);
    }

    /**
     * Recursive render child categories in main menu
     * @param int $category_parent_id
     */
    private function renderChildCategoriesByRecursive(int $category_parent_id): void
    {
        $child = array_keys($this->categoryXref, $category_parent_id);
        if (!empty($child)) {
            foreach ($child as $v) {
                # Open html level menu
                $this->htmlMainMenu .= $this->renderView(
                    'buyer/main-menu/pieces/open-level-'.$this->level.'.html.twig',
                    [ 'name' => $this->categoryName[$v], 'url' => $this->getCategoryUrl($v) ]
                );

                $this->level++;
                $this->renderChildCategoriesByRecursive($v);
                $this->level--;

                # Close html level menu
                $this->htmlMainMenu .= $this->renderView(
                    'buyer/main-menu/pieces/close-level-'.$this->level.'.html.twig',
                    [ 'name' => $this->categoryName[$v], 'slug' => $this->categorySlug[$v] ]
                );
            }
        }
    }

    /**
     * Rendering sidebar menu
     * @return Response
     */
    public function getSidebarMenuTree()
    {
        if(empty($this->sidebarMenu))
            $this->renderChildCategoriesByRecursiveSidebarMenu(0);

        return new Response($this->sidebarMenu);
    }

    /**
     * Recursive render child categories in main menu
     * @param int $category_parent_id
     */
    private function renderChildCategoriesByRecursiveSidebarMenu(int $category_parent_id): void
    {
        $child = array_keys($this->categoryXref, $category_parent_id);
        if (!empty($child)) {
            if($this->level)
                $this->sidebarMenu .= '<ul>';
            foreach ($child as $v) {
                # Open html level menu
                $this->sidebarMenu .= '<li><a href="'.$this->getCategoryUrl($v).'">'.$this->categoryName[$v].'</a>';

                $this->level++;
                $this->renderChildCategoriesByRecursiveSidebarMenu($v);
                $this->level--;

                $this->sidebarMenu .= '</li>';

            }
            if($this->level)
                $this->sidebarMenu .= '</ul>';
        }
    }

    public function getAllChildCategories(int $category_id): array
    {
        $this->allChildCategories = [];
        $this->getChildCategoriesRecursive($category_id);

        return $this->allChildCategories;
    }

    private function getChildCategoriesRecursive(int $parent_category_id): void
    {
        $this->allChildCategories[] = $parent_category_id;

        $child = array_keys($this->categoryXref, $parent_category_id);
        if(!empty($child))
            foreach ($child as $v)
                $this->getChildCategoriesRecursive($v);
    }

    /**
     * Return options array for seller category select
     * @return array
     */
    public function getOptionsForCategorySelect(string $flipForFormType = '', string $placeholder = ''): ?array
    {
        $this->level = 0;
        if(empty($this->sellerCategories))
            $this->getChildOptionsCategoriesByRecursive(0);

        $result = $placeholder ? [0 => $placeholder] + $this->sellerCategories : $this->sellerCategories;

        return !$flipForFormType ? $result : array_flip($result);
    }

    /**
     * Recursive find child categories for seller category select
     * @param int $category_parent_id
     */
    private function getChildOptionsCategoriesByRecursive(int $category_parent_id): void
    {
        $padding = '-- ';
        $child = array_keys($this->categoryXref, $category_parent_id);
        if (!empty($child)) {
            foreach ($child as $v) {

                #$this->sellerCategories[$v] = $this->level.str_pad($padding, $this->level).$this->categoryName[$v];
                $this->sellerCategories[$v] = str_pad($this->categoryName[$v],
                    mb_strlen($this->categoryName[$v]) + $this->level * mb_strlen($padding), $padding,
                    STR_PAD_LEFT);;

                $this->level++;
                $this->getChildOptionsCategoriesByRecursive($v);
                $this->level--;


            }
        }
    }


    /**
     * Called once during initialization of the service
     */
    private function getAllCategories(): self
    {
        # Через Category Entity
        $categories = $this->em->getRepository(Category::class)->findBy(
            ['publish' => 1],
            ['ordering' => 'ASC', 'name' => 'ASC']
        );

        foreach ($categories as $c) {
            $this->categoryXref[$c->getCategoryId()] = $c->getParentId();
            $this->categoryName[$c->getCategoryId()] = $c->getName();
            $this->categorySlug[$c->getCategoryId()] = $c->getSlug();
        }

        # Через прямой SQL запрос
        /*

         $conn = $this->em->getConnection();

        $sql = 'SELECT * FROM category
        WHERE publish = :publish
        ORDER BY ordering, name';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['publish'=>1]);

        $categories =  $stmt->fetchAll();

        /*foreach($categories as $c) {
            $this->categoryXref[$c['category_id']] = $c['parent_id'];
            $this->categoryName[$c['category_id']] = $c['name'];
            $this->categorySlug[$c['category_id']] = $c['slug'];
        }*/

        return $this;

    }

    /**
     * Hit database once
     * @param string $flipForFormType
     * @param string $placeholder
     * @return array
     */
    public function getStates(string $flipForFormType = '', string $placeholder = ''): ?array
    {
        if (empty($this->states)) {
            # Через State Entity
            $states = $this->em->getRepository(State::class)->findBy(
                [],
                ['stateName' => 'ASC']
            );

            foreach ($states as $s) {
                $this->states[$s->getStateId()] = $s->getStateName();
            }
        }

        $result = $placeholder ? [0 => $placeholder] + $this->states : $this->states;

        return !$flipForFormType ? $result : array_flip($result);
    }

    /**
     * @param $productId
     * @return string
     */
    public function getProductPhotoPath($productId) {
        $str = str_split(md5($productId), 2);
        $path = '/uploads/product/' . $str[0] . '/' . $str[1] . '/' . $str[2] . '/' . $productId ;
        if(!file_exists($this->publicDir . $path))
            mkdir($this->publicDir . $path, 0755, true);

        return $path;
    }

}