<?php
/**
 * User helper for get user fields values
 */

namespace App\Service;

use App\Entity\Form\FormValue;
use Doctrine\ORM\EntityManagerInterface;


class UserService
{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Get client types for user profile
     * @param string $placeholder
     * @return array
     */
    public function getClientTypes(string $placeholder = 'placeholder.clientTypeId'): ?array
    {
        $result = [0 => $placeholder];
        # Через Form Entity
        $res = $this->em->getRepository(FormValue::class)->findBy(
            ['formFieldId'=>1],
            ['formValueName' => 'ASC']
        );

        foreach ($res as $r) {
            $result[$r->getFormValueId()] = $r->getFormValueName();
        }

        return array_flip($result);
    }

    /**
     * Get user titles for user profile
     * @param string $placeholder
     * @return array
     */
    public function getUserTitles(string $placeholder = 'placeholder.userTitleId'): ?array
    {
        $result = [0 => $placeholder];
        # Через FormValue Entity
        $res = $this->em->getRepository(FormValue::class)->findBy(
            ['formFieldId'=>2]/*,
            ['formValueName' => 'ASC']*/
        );

        foreach ($res as $r) {
            $result[$r->getFormValueId()] = $r->getFormValueName();
        }

        return array_flip($result);
    }

    /**
     * Get delivery types for user profile
     * @param string $placeholder
     * @return array
     */
    public function getDeliveryTypes(string $placeholder = 'placeholder.deliveryTypeId'): ?array
    {
        $result = [0 => $placeholder];
        # Через FormValue Entity
        $res = $this->em->getRepository(FormValue::class)->findBy(
            ['formFieldId'=>3]/*,
            ['formValueName' => 'ASC']*/
        );

        foreach ($res as $r) {
            $result[$r->getFormValueId()] = $r->getFormValueName();
        }

        return array_flip($result);
    }
}