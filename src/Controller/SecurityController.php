<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/", name="app_book_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        # If user already login - redirect to cPanel page
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirect($this->generateUrl( 'app_book_dashboard'));
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        if($error)
            $this->addFlash('error', $error->getMessage());

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('book/login.html.twig', ['last_username' => $lastUsername]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */

    public function logout()
    {
        #throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }
}
