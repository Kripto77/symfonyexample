<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Manage users with roles by admin
 *
 * @Route("/user")
 * @IsGranted("ROLE_ADMIN")
 */

class UserController extends AbstractController
{
    private $defaultPlainPassword = 'adm@312';
    /** @var User */
    private $currentUser;
    private $userRepository;
    private $passwordEncoder;
    private $translator;

    public function __construct(UserRepository $userRepository, UserPasswordEncoderInterface $passwordEncoder,
        TranslatorInterface $translator)
    {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->translator = $translator;
    }

    /**
     * @Route("/", name="app_book_user_listing")
     */
    public function listing()
    {
        $users = $this->userRepository->getAllUsers();

        return $this->render('book/user/listing.html.twig', [
            'paginator' => $users,
            'ajax' => [
                'urlOnOff' => 'app_book_user_ajax_on_off',
            ],
        ]);
    }

    /**
     * @Route("/add", name="app_book_user_add")
     * @param Request $request
     * @return Response
     */
    public function add(Request $request)
    {
        $this->currentUser = new User();
        $this->currentUser = new User();
        $form = $this->createForm(UserType::class, $this->currentUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->userChangeRoles()->userCheckFirstPassword()->userChangePassword();

            $db = $this->getDoctrine()->getManager();
            $db->persist($this->currentUser);
            $db->flush();

            $this->addFlash('success', 'message.user.added_successfully');

            return $this->redirectToRoute('app_book_user_listing');
        }

        return $this->render('book/user/add_user.html.twig', [
            'user' => new User(),
            'userRoles' => $this->userRepository->getUserTreeRoles(),
            'form' => $form->createView(),
            'controlButtons' => $this->getControlButtons($form),
            'ajax' => [
                'urlOnOff' => 'app_book_user_ajax_on_off',
            ],
        ]);
    }

    /**
     * @Route("/edit/{userId}", name="app_book_user_edit")
     * @param Request $request
     * @param int $userId
     * @return Response
     */
    public function edit(Request $request, int $userId)
    {
        $this->currentUser = $this->userRepository->find($userId);

        $form = $this->createForm(UserType::class, $this->currentUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->userChangeRoles()->userChangePassword();

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'message.user.updated_successfully');

            return $this->redirectToRoute('app_book_user_listing');
        }

        return $this->render('book/user/edit_user.html.twig', [
            'user' => $this->currentUser,
            'userRoles' => $this->userRepository->getUserTreeRoles(),
            'form' => $form->createView(),
            'controlButtons' => $this->getControlButtons($form),
            'ajax' => [
                'urlOnOff' => 'app_book_user_ajax_on_off',
            ],
        ]);
    }

    /**
     * @param $form
     * @return array
     */
    private function getControlButtons($form): array
    {
        return [
            'form_name' => $form->getName(),
            'buttons' => [ 'save', 'saveAndExit', 'cancel' ],
        ];
    }

    /**
     * @return UserController
     */
    private function userChangeRoles(): self
    {
        $this->currentUser->setRoles($this->userRepository->processRoles());

        return $this;
    }

    /**
     * @return UserController
     */
    private function userChangePassword(): self
    {
        if(!empty($this->currentUser->getPlainPassword())) {
            $password = $this->passwordEncoder->encodePassword($this->currentUser, $this->currentUser->getPlainPassword());
            $this->currentUser->setPassword($password);
        }

        return $this;
    }

    /**
     * If empty plainPassword - set default password
     * @return UserController
     */
    private function userCheckFirstPassword(): self
    {
        if(empty($this->currentUser->getPlainPassword())) {
            $this->currentUser->setPlainPassword($this->defaultPlainPassword);

            $message = $this->translator->trans(
                'message.user.set_default_password',
                ['password_value' => $this->defaultPlainPassword],
                'security');

            $this->addFlash('info', $message);
        }

        return $this;
    }
}