<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DashboardController extends AbstractController
{
    /**
     * @Route("/dashboard", name="app_book_dashboard")
     */
    public function dashboard()
    {
        return $this->render('book/dashboard/dashboard.html.twig', [
            'user' => $this->getUser(),
            //'products' => $this->getProductsData()
        ]);
    }

}