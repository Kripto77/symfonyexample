<?php
/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Twig;

#use App\Service\HelperService;
#use Symfony\Component\Intl\Locales;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Extension\AbstractExtension;
#use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * This Twig extension adds a new 'md2html' filter to easily transform Markdown
 * contents into HTML contents inside Twig templates.
 *
 * See https://symfony.com/doc/current/templating/twig_extension.html
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 * @author Julien ITARD <julienitard@gmail.com>
 */
class AppExtension extends AbstractExtension
{
    /*private $parser;
    private $localeCodes;
    private $locales;*/
    private $helperService;
    private $security;
    private $parameterBag;

    public function __construct(/*HelperService $helperService,*/ Security $security, ParameterBagInterface $parameterBag)
    {
        #$this->helperService = $helperService;
        $this->security = $security;
        $this->parameterBag = $parameterBag;

        /*$localeCodes = explode('|', $locales);
        sort($localeCodes);
        $this->localeCodes = $localeCodes;*/
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters(): array
    {
        return [
            #new TwigFilter('formatPrice', [$this, 'formatPrice'], ['is_safe' => ['html']]),
            #new TwigFilter('md2html', [$this, 'markdownToHtml'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('format_price',[$this, 'formatPrice'], ['is_safe' => ['html']]),
            new TwigFunction('get_product_photo_path',[$this, 'getProductPhotoPath'], ['is_safe' => ['html']]),
            new TwigFunction('has_any_role',[$this, 'hasAnyRole'], ['is_safe' => ['html']]),
            new TwigFunction('has_any_child_role',[$this, 'hasAnyChildRole'], ['is_safe' => ['html']]),
            #new TwigFunction('locales', [$this, 'getLocales']),
        ];
    }

    /**
     * Customize price view.
     * @param float $price
     * @return string
     */
    public function formatPrice(float $price): string
    {
        return $price ? '₹'.number_format($price, 0, '', ',') : 'No price';
    }

    /**
     * Customize price view.
     * @param float $price
     * @return string
     */
    public function getProductPhotoPath(int $productId): string
    {
        return $this->helperService->getProductPhotoPath($productId);
    }

    /**
     * Check if user have any roles from array
     * @param array $roles
     * @return bool
     */
    public function hasAnyRole(array $roles): bool
    {
        foreach ($roles as $role)
            if ($this->security->isGranted($role))
                return true;

        return false;
    }

    /**
     * Check if user have any child role from array
     * @param array $parentRoles
     * @return bool
     */
    public function hasAnyChildRole(array $parentRoles): bool
    {
        $role_hierarchy = $this->parameterBag->get('security.role_hierarchy.roles');

        $roles = [];
        foreach($parentRoles as $parentRole) {
            $roles[] =  $parentRole;
            if (!empty($role_hierarchy[$parentRole]))
                $roles = array_merge($roles, $role_hierarchy[$parentRole]);
        }

        $user = $this->security->getUser();

        return array_intersect($user->getRoles(), $roles) ? true : false;
    }

    /**
     * Transforms the given Markdown content into HTML content.

    public function markdownToHtml(string $content): string
    {
        return $this->parser->toHtml($content);
    }

    /**
     * Takes the list of codes of the locales (languages) enabled in the
     * application and returns an array with the name of each locale written
     * in its own language (e.g. English, Français, Español, etc.).

    public function getLocales(): array
    {
        if (null !== $this->locales) {
            return $this->locales;
        }

        $this->locales = [];
        foreach ($this->localeCodes as $localeCode) {
            $this->locales[] = ['code' => $localeCode, 'name' => Locales::getName($localeCode, $localeCode)];
        }

        return $this->locales;
    }*/
}
