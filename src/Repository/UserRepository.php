<?php

namespace App\Repository;

use App\Entity\User;
use App\Service\PaginatorService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    private $paginatorService;
    private $parameterBag;

    public function __construct(RegistryInterface $registry, PaginatorService $paginatorService,
        ParameterBagInterface $parameterBag)
    {
        parent::__construct($registry, User::class);

        $this->paginatorService = $paginatorService;
        $this->parameterBag = $parameterBag;
    }

    /**
     * Get ALL users
     */
    public function getAllUsers()
    {
        $query = $this->createQueryBuilder('u')
            ->orderBy('u.id', 'ASC')
        ;

        $paginator = $this->paginatorService->createPaginator($query);

        return $paginator;

    }

    /**
     * Get user by userId
     * @param int $userId
     * @return User|null
     */
    public function getUserById(int $userId): ?User
    {

    }

    /**
     * Try to set user roles tree
     */
    public function getUserTreeRoles(): array
    {
        return $this->parameterBag->get('security.role_hierarchy.roles');
    }

    /**
     * Process user_roles[]
     * @return array
     */
    public function processRoles(): array
    {
        $checkedRoles = [];

        $userRoles = isset($_POST['user_roles']) && is_array($_POST['user_roles']) ? $_POST['user_roles'] : [];

        $allRoles = $this->getRolesInFlattenArray();

        foreach ($userRoles as $userRole) {
            if(in_array($userRole, $allRoles))
                $checkedRoles[] = $userRole;
        }

        return $checkedRoles;
    }

    private function getRolesInFlattenArray()
    {
        $roles = $this->parameterBag->get('security.role_hierarchy.roles');
        $flattenArrayRoles = [];

        foreach ($roles as $role => $childRoles) {
            $flattenArrayRoles[] = $role;
            if(is_array($childRoles)) {
                foreach($childRoles as $childRole) {
                    $flattenArrayRoles[] = $childRole;
                }
            }
        }
        /*dump($roles);
        $iterator = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($roles), \RecursiveIteratorIterator::SELF_FIRST);
        $flatten_array = array_values(iterator_to_array($iterator,true));
        // display $flatten_array
        dump($flatten_array);*/

        return array_unique($flattenArrayRoles);
    }


    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
